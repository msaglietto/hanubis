/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.auratus.logic;

import ar.com.auratus.bean.Documento;
import ar.com.auratus.dao.DocumentoDAO;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Hernan
 */
public class CrawlerController {

    private DocumentoDAO documentoDAO;
    public static ArrayList<Documento> documentoList = new ArrayList<Documento>();
    public static ArrayList<Documento> crawledSites = new ArrayList<Documento>();

    public CrawlerController(String semilla) {
        documentoDAO = new DocumentoDAO();

        //Documento documento = documentoDAO.getUltimoInsert();
        Documento documento = null;
        if (documento == null) {
            documento = new Documento();
            documento.setUrl(semilla);
            documento.setAIndexar(Boolean.TRUE);
            documentoList.add(documento);            
        } else {
            documentoList.add(documento);
        }
    }

    public void crawlerProcess() throws InterruptedException {
        ExecutorService threadPool = Executors.newFixedThreadPool(30);

        while (true) {
            Documento doc = next();
            synchronized (documentoList) {
                if (doc != null) {
                    System.out.println(CrawlerController.documentoList.size() + "---Documento Nuevo--> " + doc.getUrl());
                    Crawler crawler = new Crawler(doc, documentoDAO);
                    threadPool.execute(crawler);
                    doc = next();
                    //threadPool.shutdown();
                } else {
                    Thread.sleep(3000);
                }
            }
        }
    }

    public static void add(Documento site) {
        synchronized (CrawlerController.class) {
            if (!inList(site)) {
                documentoList.add(site);
                CrawlerController.class.notifyAll();
            }
        }
    }

    /**
     * Get next site to crawl. Can return null (if nothing to crawl)
     */
    public static Documento next() {
        if (documentoList.isEmpty()) {
            return null;
        }
        synchronized (CrawlerController.class) {
            // Need to check again if size has changed
            if (documentoList.size() > 0) {
                Documento s = documentoList.get(0);
                documentoList.remove(s);
                crawledSites.add(s);
                return s;
            }
            return null;
        }
    }

    public static boolean inList(Documento doc) {
        for (Documento d : CrawlerController.documentoList) {
            if (d.getUrl().equals(doc.getUrl())) {
                return true;
            }
        }
        return false;
    }
}
