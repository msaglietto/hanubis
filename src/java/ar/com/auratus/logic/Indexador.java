package ar.com.auratus.logic;

import ar.com.auratus.bean.Documento;
import ar.com.auratus.bean.Posteo;
import ar.com.auratus.bean.Vocabulario;
import ar.com.auratus.dao.DocumentoDAO;
import ar.com.auratus.dao.VocabularioDAO;
import ar.com.auratus.service.TratamientoCadena;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

public class Indexador extends Thread {

    private VocabularioDAO vocabularioDAO;
    private Documento documento;
    private DocumentoDAO docuemntoDAO;

    public Indexador(VocabularioDAO vocabularioDAO, DocumentoDAO docuemntoDAO, Documento documento) {
        this.vocabularioDAO = vocabularioDAO;
        this.documento = documento;
        this.docuemntoDAO = docuemntoDAO;
    }

    @Override
    public void run() {        
        HashMap<String, Posteo> tablaPosteoTemporal = new HashMap<String, Posteo>();
        List<Vocabulario> saveOrUpdateVocabulario = new ArrayList<Vocabulario>();

        String cadenaAIndexar = TratamientoCadena.tratarTextoIndexacion(documento.getDocumento());

        StringTokenizer tokenizerPalabras = new StringTokenizer(cadenaAIndexar); // se divide el texto en palabras
        // se procede a trabajar con cada palabra del documento en un ciclo while
        while (tokenizerPalabras.hasMoreElements()) {
            String palabra = tokenizerPalabras.nextElement().toString();

            /* Se verifica si es una palabra vacia. En caso positivo no debemos trabajar con esa palabra */
            if (!TratamientoCadena.esPalabraVacia(palabra)) {

                /* Por cada palabra del documento se debe crear un posteo para incluirlo en la lista
                 * de posteo del vocabulario. Para ello se usa el metodo crearPosteo. El nuevo posteo
                 * se agrega en tablaPosteoTemporal y se puede obtener realizando un get con la palabra
                 * del posteo. */
                crearPosteo(tablaPosteoTemporal, palabra, documento);

                /* Se debe verificar si el Vocabulario en memoria contiene a la palabra que se esta analizando.
                 * En caso positivo se obtiene el objeto Vocabulario correspondiente a la palabra.
                 * En caso negativo se debe crear un nuevo objeto Vocabulario y agregarlo al hashVocabulario que
                 * esta en memoria.
                 */
                if (vocabularioDAO.getHashVocabulario().containsKey(palabra)) {
                    Vocabulario vocabulario = vocabularioDAO.getHashVocabulario().get(palabra);
                    Posteo posteo = tablaPosteoTemporal.get(palabra);

                    // Se actualizan los valores de la frecuencia maxima del vocabulario
                    if (posteo.getFrecuencia() > vocabulario.getFrecuenciaMaxima()) {
                        vocabulario.setFrecuenciaMaxima(posteo.getFrecuencia());
                    }
                    
                    if(posteo.getVocabularioID()==null){
                        posteo.setVocabularioID(vocabulario);
                        vocabulario.setCantidadDocumentos(vocabulario.getCantidadDocumentos()+1);
                    }                    
                    saveOrUpdateVocabulario.add(vocabulario);
                } else {
                    Posteo posteo = tablaPosteoTemporal.get(palabra);                    

                    Vocabulario vocabulario = new Vocabulario();
                    vocabulario.setPalabra(palabra);
                    vocabulario.setCantidadDocumentos(1);
                    vocabulario.setFrecuenciaMaxima(posteo.getFrecuencia());
                    vocabulario.getPosteoCollection().add(posteo);
                    
                    posteo.setVocabularioID(vocabulario);
                   
                    vocabularioDAO.getHashVocabulario().put(palabra, vocabulario);
                    saveOrUpdateVocabulario.add(vocabulario);
                }
            }


        }
        //vocabularioDAO.saveOrUpdateHashVocabulario();
        vocabularioDAO.saveOrUpdateVocabulariosYPosteos(saveOrUpdateVocabulario,tablaPosteoTemporal.values());
        documento.setAIndexar(false);
        docuemntoDAO.update(documento);
    }


    private static void crearPosteo(HashMap<String, Posteo> tablaPosteoTemporal, String palabra, Documento documento) {
        //antes buscar si existe la palabra antes de guardarla
        if (!tablaPosteoTemporal.containsKey(palabra)) {
            //primera vez que entra una palabra le asigno el valor 1
            Posteo posteo = new Posteo();
            posteo.setDocumentoID(documento);
            posteo.setFrecuencia(1);
            tablaPosteoTemporal.put(palabra, posteo);
        } else {
            Posteo posteo = tablaPosteoTemporal.get(palabra);
            Integer frecuencia = posteo.getFrecuencia();
            frecuencia++;
            posteo.setFrecuencia(frecuencia);
        }
    }
}
