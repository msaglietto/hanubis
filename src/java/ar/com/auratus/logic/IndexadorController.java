/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.auratus.logic;

import ar.com.auratus.bean.Documento;
import ar.com.auratus.dao.DocumentoDAO;
import ar.com.auratus.dao.VocabularioDAO;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 *
 * @author mauricio
 */
public class IndexadorController {

    private DocumentoDAO documentoDAO;
    private VocabularioDAO vocabularioDAO;

    public IndexadorController() {
        documentoDAO = new DocumentoDAO();
        vocabularioDAO = new VocabularioDAO();
        vocabularioDAO.initHashVocabulario();
    }

    public void indexProcess() {
        ExecutorService threadPool = Executors.newFixedThreadPool(1);
        List<Documento> listaDocumentos = documentoDAO.getAllDocumentToIndex();
        for (Documento documento : listaDocumentos) {
            Indexador indexador = new Indexador(vocabularioDAO, documentoDAO, documento);
            threadPool.execute(indexador);
        }
        threadPool.shutdown();
    }
}
