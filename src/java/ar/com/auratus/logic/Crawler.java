/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.auratus.logic;

import ar.com.auratus.bean.Documento;
import ar.com.auratus.dao.DocumentoDAO;
import ar.com.auratus.service.ParserHTML;
import java.util.ArrayList;
import java.util.Iterator;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

/**
 *
 * @author Hernan
 */
public class Crawler extends Thread {

    private Documento documento;
    private DocumentoDAO docDao;

    public Crawler(Documento documento, DocumentoDAO docDao) {
        this.documento = documento;
        this.docDao = docDao;
    }

    @Override
    public void run() {
        getDocumento();
    }

    public void getDocumento() {
        Document document = ParserHTML.getDocument(documento.getUrl());
        if (document != null) {
            ArrayList<String> urlList = ParserHTML.getHref(document);
            Iterator iterator = urlList.iterator();
            while (iterator.hasNext()) {
                String url = (String) iterator.next();
                url = checkUrl(documento.getUrl(), url).trim();
                if (docDao.noExistUrl(url) && limiteDominio(url) && noEsImagen(url) && noEsReferencia(url)) {
                    Documento doc = new Documento();
                    doc.setAIndexar(true);
                    doc.setUrl(url);
                    CrawlerController.add(doc);
                    //System.out.println(doc.getUrl());
                }
            }
            documento.setTitulo(document.getElementsByTag("title").text());
            Elements descripciones = document.select("[name='description']");
            if (!descripciones.isEmpty()) {
                documento.setDescripcion(descripciones.text());
            } else {
                documento.setDescripcion(document.text().substring(0, 300));
            }
            documento.setDocumento(document.text());
            docDao.save(documento);
        }
    }

    public String checkUrl(String urlParent, String findUrl) {
        if (findUrl.startsWith("http://") || findUrl.startsWith("www.")) {
            return findUrl;
        } else {
            if (findUrl.contains("/n/")) {
                return "http://es.wikipedia.org" + findUrl;
            }
            if (findUrl.contains(":") || urlParent.contains("/Wikipedia:")) {
                return "http://es.wikipedia.org" + findUrl;
            }
            if (findUrl.startsWith("/wiki/")) {
                return "http://es.wikipedia.org" + findUrl;
            }
            findUrl = findUrl.replaceFirst("/wiki/", "/");
            //findUrl = findUrl.replaceFirst("/Wikipedia:", "/");
            return urlParent.concat(findUrl);
        }
    }

    public boolean limiteDominio(String url) {
        if (url.startsWith("http://es.wikipedia.org")) {
            return true;
        } else {
            return false;
        }
    }

    public boolean noEsImagen(String url) {
        if (!url.contains(".svg") && !url.contains(".jpg") && !url.contains(".gif") && !url.contains(".png")
                && !url.contains(".SVG") && !url.contains(".JPG") && !url.contains(".GIF") && !url.contains(".PNG") 
                && !url.contains("=edit"))  {
            return true;
        } else {
            return false;
        }
    }

    public boolean noEsReferencia(String url) {
        if (url.contains("#")) {
            return false;
        } else {
            return true;
        }
    }
}
