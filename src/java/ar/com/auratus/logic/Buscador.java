package ar.com.auratus.logic;

import ar.com.auratus.bean.Posteo;
import ar.com.auratus.dao.VocabularioDAO;
import ar.com.auratus.search.BusquedaDocumento;
import ar.com.auratus.search.BusquedaVocabulario;
import ar.com.auratus.service.TratamientoCadena;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class Buscador {

    public static List<BusquedaDocumento> buscar(String consulta) {
        String consultaLimpia = TratamientoCadena.tratarTextoBusqueda(consulta);
        VocabularioDAO vocabularioDAO = new VocabularioDAO();
        List<BusquedaVocabulario> palabrasBusqueda = vocabularioDAO.obtenerConsultaBusquedaOrdenadaPorNr(consultaLimpia);

        HashMap<String, BusquedaDocumento> hashTemporal = new HashMap<String, BusquedaDocumento>();

        List<BusquedaDocumento> resultadosBusqueda = new ArrayList<BusquedaDocumento>();

        for (BusquedaVocabulario busquedaVocabulario : palabrasBusqueda) {
            for (Posteo posteo : busquedaVocabulario.getVocabulario().getPosteoCollection()) {
                if (hashTemporal.containsKey(posteo.getDocumentoID().getUrl())) {
                    BusquedaDocumento busquedaDocumentoGuardada = hashTemporal.get(posteo.getDocumentoID().getUrl());

                    Integer pesoSumar = Math.round(posteo.getFrecuencia() * busquedaVocabulario.getProporcionCoincidencia() * BusquedaDocumento.PESO_EXACTO);
                    Integer nuevoPeso = (busquedaDocumentoGuardada.getPeso() + pesoSumar);
                    busquedaDocumentoGuardada.setPeso(nuevoPeso);
                    busquedaDocumentoGuardada.addBusquedaVocabularioEnLista(busquedaVocabulario);
                    hashTemporal.put(posteo.getDocumentoID().getUrl(), busquedaDocumentoGuardada);
                } else {
                    BusquedaDocumento busquedaDocumento = new BusquedaDocumento();
                    busquedaDocumento.setDocumento(posteo.getDocumentoID());

                    Integer pesoSumar = Math.round(posteo.getFrecuencia() * busquedaVocabulario.getProporcionCoincidencia() * BusquedaDocumento.PESO_EXACTO);
                    busquedaDocumento.setPeso(pesoSumar);

                    busquedaDocumento.addBusquedaVocabularioEnLista(busquedaVocabulario);
                    resultadosBusqueda.add(busquedaDocumento);
                    hashTemporal.put(posteo.getDocumentoID().getUrl(), busquedaDocumento);
                }
            }
        }

        Collections.sort(resultadosBusqueda);
        Collections.reverse(resultadosBusqueda);

        return resultadosBusqueda;
    }
}
