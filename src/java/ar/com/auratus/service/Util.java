package ar.com.auratus.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Util {

    public static String resaltarKeywords(String texto, List<String> keywords) {
        for (String keyword : keywords) {
            if (!keyword.equals("")) {
                texto.replace(keyword, "<b>"+keyword+"</b>");
            }
        }
        return texto;
    }

    public static String resaltarKeywordsPrueba(String texto, List<String> keywords) {
        String startTag = "<b>";
        String endTag = "</b>";

        List<String> nuevaListaKeywords = new ArrayList<String>();

        for (String keyword : keywords) {
            if (!keyword.equals("")) {
                Matcher matcherKeyword = obtenerMatcherCaseInsensitive(keyword, texto);
                while (matcherKeyword.find()) {
                    nuevaListaKeywords.add(matcherKeyword.group());
                }
            }
        }

        String textoNuevo;
        String textoCorto = "";
        int cortarHasta;
        int renglones = 0;
        for (String string : nuevaListaKeywords) {
            if (!string.equals("")) {

                if (texto.indexOf(string) > -1) {

                    int indPal = texto.indexOf(string);
                    if (indPal - 10 > 0) {
                        textoNuevo = texto.substring(indPal - 10, texto.length());
                    } else {
                        textoNuevo = texto.substring(indPal, texto.length());
                    }

                    if (textoNuevo.length() - 1 >= 100) {
                        cortarHasta = 100;
                    } else {
                        cortarHasta = textoNuevo.length();
                    }

                    textoCorto = textoCorto + "..." + textoNuevo.substring(0, cortarHasta) + "...<br>";
                    int cortarDesde =  textoNuevo.substring(0, cortarHasta).length() - 1;
                    if (cortarDesde < textoNuevo.length() - 1) {
                        texto = textoNuevo.substring(cortarDesde, textoNuevo.length() - 1);
                    } else {
                        texto = textoNuevo;
                    }

                    renglones++;
                    if (renglones > 5) {
                        break;
                    }
                }
            }
        }

        if(textoCorto.length() == 0 ){
            textoCorto = texto.substring(0, 400);
        }
        
        Set<String> set = new HashSet<String>(nuevaListaKeywords);
        for (String keyword : set) {
            if (!keyword.equals("")) {
                StringBuilder hResult = new StringBuilder();
                hResult.append(startTag);
                hResult.append(keyword);
                hResult.append(endTag);
                textoCorto = textoCorto.replace(keyword, hResult.toString());
            }
        }

        return textoCorto;
    }

    private static Matcher obtenerMatcherCaseInsensitive(String er, String cadena) {
        Pattern regex = Pattern.compile(er, Pattern.CASE_INSENSITIVE);
        Matcher regexMatcher = regex.matcher(cadena);
        return regexMatcher;
    }
}
