/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.auratus.service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 *
 * @author Hernan
 */
public class ParserHTML {

    public static Document getDocument(String url) {
        File input = new File(url);
        try {
            //Document doc = Jsoup.parse(input, "UTF-8");
            Document doc= Jsoup.connect(url).timeout(10000).get();
            return doc;
        } catch (IOException ex) {
            //Logger.getLogger(ParserHTML.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public static ArrayList<String> getHref(Document doc) {
        if (doc == null) {
            return null;
        } else {
            ArrayList<String> href = new ArrayList<String>();
            //Elements content = doc.getElementsByTag("a");
            Elements content = doc.select("a[href]");
            for (Element link : content) {
                href.add(link.attr("href"));
            }
            return href;
        }
    }
}
