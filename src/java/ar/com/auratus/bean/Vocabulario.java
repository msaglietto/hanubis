/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.auratus.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "vocabulario")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Vocabulario.findAll", query = "SELECT v FROM Vocabulario v")})
public class Vocabulario implements Serializable, Comparable<Vocabulario>{
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Size(max = 45)
    @Column(name = "palabra")
    private String palabra;
    @Column(name = "frecuenciaMaxima")
    private Integer frecuenciaMaxima;
    @Column(name = "cantidadDocumento")
    private Integer cantidadDocumentos;
    @OneToMany(mappedBy = "vocabularioID", fetch= FetchType.LAZY)
    private Collection<Posteo> posteoCollection = new ArrayList<Posteo>();

    public Vocabulario() {
    }

    public Vocabulario(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPalabra() {
        return palabra;
    }

    public void setPalabra(String palabra) {
        this.palabra = palabra;
    }

    public Integer getFrecuenciaMaxima() {
        return frecuenciaMaxima;
    }

    public void setFrecuenciaMaxima(Integer frecuenciaMaxima) {
        this.frecuenciaMaxima = frecuenciaMaxima;
    }

    public Integer getCantidadDocumentos() {
        return cantidadDocumentos;
    }

    public void setCantidadDocumentos(Integer cantidadDocumento) {
        this.cantidadDocumentos = cantidadDocumento;
    }

    @XmlTransient
    public Collection<Posteo> getPosteoCollection() {
        return posteoCollection;
    }

    public void setPosteoCollection(Collection<Posteo> posteoCollection) {
        this.posteoCollection = posteoCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Vocabulario)) {
            return false;
        }
        Vocabulario other = (Vocabulario) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ar.com.auratus.bean.Vocabulario[ id=" + id + " ]";
    }

    @Override
    public int compareTo(Vocabulario v) {
        if (v != null) {
            if (this.getCantidadDocumentos() > v.getCantidadDocumentos()) {
                return 1;
            } else if (this.getCantidadDocumentos() == v.getCantidadDocumentos()) {
                return 0;
            } else {
                return -1;
            }
        } else {
            return -1;
        }
    }
    
}
