/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.auratus.bean;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mauricio
 */
@Entity
@Table(name = "posteo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Posteo.findAll", query = "SELECT p FROM Posteo p")})
public class Posteo implements Serializable, Comparable<Posteo> {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Long id;
    @Column(name = "frecuencia")
    private Integer frecuencia;
    @JoinColumn(name = "vocabularioID", referencedColumnName = "id")
    @ManyToOne(fetch= FetchType.EAGER)
    private Vocabulario vocabularioID;
    @JoinColumn(name = "documentoID", referencedColumnName = "id")
    @ManyToOne(fetch= FetchType.EAGER)
    private Documento documentoID;

    public Posteo() {
    }

    public Posteo(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getFrecuencia() {
        return frecuencia;
    }

    public void setFrecuencia(Integer frecuencia) {
        this.frecuencia = frecuencia;
    }

    public Vocabulario getVocabularioID() {
        return vocabularioID;
    }

    public void setVocabularioID(Vocabulario vocabularioID) {
        this.vocabularioID = vocabularioID;
    }

    public Documento getDocumentoID() {
        return documentoID;
    }

    public void setDocumentoID(Documento documentoID) {
        this.documentoID = documentoID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Posteo)) {
            return false;
        }
        Posteo other = (Posteo) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ar.com.auratus.bean.Posteo[ id=" + id + " ]";
    }

    @Override
    public int compareTo(Posteo p) {
        if (p != null) {
            if (this.getFrecuencia() > p.getFrecuencia()) {
                return 1;
            } else if (this.getFrecuencia() == p.getFrecuencia()) {
                return 0;
            } else {
                return -1;
            }
        } else {
            return -1;
        }
    }
    
}
