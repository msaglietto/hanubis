/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.auratus.controller;

import ar.com.auratus.bean.Documento;
import ar.com.auratus.logic.Buscador;
import ar.com.auratus.search.BusquedaDocumento;
import ar.com.auratus.service.TratamientoCadena;
import ar.com.auratus.service.Util;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 *
 * @author mauricio
 */
public class SearchController {

    private String query;
    private List<Documento> documentoList = new ArrayList<Documento>();
    private Double tiempoBusqueda;

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public List<Documento> getDocumentoList() {
        return documentoList;
    }

    public void setDocumentoList(List<Documento> documentoList) {
        this.documentoList = documentoList;
    }

    public Double getTiempoBusqueda() {
        return tiempoBusqueda;
    }

    public void setTiempoBusqueda(Double timepoBusqueda) {
        this.tiempoBusqueda = timepoBusqueda;
    }
    
    

    public String searchActionButton() {
        searchButtonListener();
        return "search";
    }

    public void searchButtonListener() {
        double startTime = System.currentTimeMillis();

        String keyword = this.query;

        List<BusquedaDocumento> listaBusquedaDocumento = Buscador.buscar(this.query);
        tiempoBusqueda = System.currentTimeMillis() - startTime;

        if (tiempoBusqueda != 0) {
            tiempoBusqueda = (double) tiempoBusqueda / 1000;
        }

        documentoList = new ArrayList<Documento>();

        List<String> listaKeywords = new ArrayList<String>();
        keyword = TratamientoCadena.tratarTextoBusqueda(keyword);
        StringTokenizer tokenKeywords = new StringTokenizer(keyword);
        while (tokenKeywords.hasMoreElements()) {
            listaKeywords.add(tokenKeywords.nextToken().toString());
        }

        for (BusquedaDocumento busquedaDocumento : listaBusquedaDocumento) {
            Documento documento = busquedaDocumento.getDocumento();
            documento.setDescripcion(Util.resaltarKeywordsPrueba(busquedaDocumento.getDocumento().getDocumento(), listaKeywords));
            documentoList.add(busquedaDocumento.getDocumento());
        }

    }
}
