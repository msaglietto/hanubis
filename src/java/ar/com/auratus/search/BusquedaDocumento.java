package ar.com.auratus.search;

import ar.com.auratus.bean.Documento;
import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author mauricio
 */
public class BusquedaDocumento implements Comparable<BusquedaDocumento>  {

    private Documento documento;
    private Integer peso;
    private List<BusquedaVocabulario> listaBusquedaVocabulario = new ArrayList<BusquedaVocabulario>();
    private boolean listaBusquedaVocabularioExacta;

    public static final short PESO_EXACTO = 20;
    public static final short PESO_SUBSTRING = 5;

    public Documento getDocumento() {
        return documento;
    }

    public void setDocumento(Documento documento) {
        this.documento = documento;
    }

    public Integer getPeso() {
        return peso;
    }

    public void setPeso(Integer peso) {
        this.peso = peso;
    }

    public List<BusquedaVocabulario> getListaBusquedaVocabulario() {
        return listaBusquedaVocabulario;
    }

    public void setListaBusquedaVocabulario(List<BusquedaVocabulario> listaBusquedaVocabulario) {
        this.listaBusquedaVocabulario = listaBusquedaVocabulario;
    }

    public boolean isListaBusquedaVocabularioExacta() {
        return listaBusquedaVocabularioExacta;
    }

    public void setListaBusquedaVocabularioExacta(boolean listaBusquedaVocabularioExacta) {
        this.listaBusquedaVocabularioExacta = listaBusquedaVocabularioExacta;
    }

    public void addBusquedaVocabularioEnLista(BusquedaVocabulario busquedaVocabulario) {
        if (busquedaVocabulario != null) {
            listaBusquedaVocabulario.add(busquedaVocabulario);
            boolean exacta = false;
            for (BusquedaVocabulario bd : listaBusquedaVocabulario) {
                if (bd.isExacto()) {
                    exacta = true;
                    break;
                }
            }
            setListaBusquedaVocabularioExacta(exacta);
        }
    }

    @Override
    public int compareTo(BusquedaDocumento bd) {
        int resultado = -1;
        if (bd != null) {
            if (this.isListaBusquedaVocabularioExacta()) {
                if (bd.isListaBusquedaVocabularioExacta()) {
                    if (this.getPeso() > bd.getPeso()) {
                        resultado = 1;
                    } else if (this.getPeso() == bd.getPeso()) {
                        resultado = 0;
                    } else {
                        resultado = -1;
                    }
                } else {
                    resultado = 1;
                }

            } else {
                if (bd.isListaBusquedaVocabularioExacta()) {
                    resultado = -1;
                } else {
                    if (this.getPeso() > bd.getPeso()) {
                        resultado = 1;
                    } else if (this.getPeso() == bd.getPeso()) {
                        resultado = 0;
                    } else {
                        resultado = -1;
                    }
                }
            }
        } else {
            resultado = -1;
        }

        return resultado;
    }

    
    @Override
    public boolean equals(Object o) {
        boolean equals = false;
        if (o != null) {
            if (o instanceof BusquedaDocumento) {
                BusquedaDocumento bd = (BusquedaDocumento)o;
                if (this.getDocumento().getUrl().equals(bd.getDocumento().getUrl())) {
                    equals = true;
                } else {
                    equals = false;
                }
            } else {
                equals = false;
            }
        } else {
            equals = false;
        }
        return equals;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + (this.documento != null ? this.documento.hashCode() : 0);
        hash = 97 * hash + this.peso;
        return hash;
    }
    
}
