package ar.com.auratus.search;

import ar.com.auratus.bean.Vocabulario;

/**
 *
 * @author mauricio
 */
public class BusquedaVocabulario {

    private Vocabulario vocabulario;
    private boolean exacto;
    private boolean subString;
    private float proporcionCoincidencia;

    public float getProporcionCoincidencia() {
        return proporcionCoincidencia;
    }

    public void setProporcionCoincidencia(float proporcionCoincidencia) {
        this.proporcionCoincidencia = proporcionCoincidencia;
    }

    public Vocabulario getVocabulario() {
        return vocabulario;
    }

    public void setVocabulario(Vocabulario vocabulario) {
        this.vocabulario = vocabulario;
    }

    public boolean isExacto() {
        return exacto;
    }

    public void setExacto(boolean exacto) {
        this.exacto = exacto;
    }

    public boolean isSubString() {
        return subString;
    }

    public void setSubString(boolean subString) {
        this.subString = subString;
    }

    /**
     *
     * @return sbf.toString()
     */
    @Override
    public String toString() {
        String nombreClase = "\n[BusquedaVocabulario] ";
        StringBuilder sbf = new StringBuilder();

        sbf.append(nombreClase);
        sbf.append("vocabulario: ");
        sbf.append(vocabulario);

        sbf.append(nombreClase);
        sbf.append("exacto: ");
        sbf.append(exacto);

        sbf.append(nombreClase);
        sbf.append("subString: ");
        sbf.append(subString);

        return sbf.toString();
    }
}
