/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.auratus.dao;

import java.lang.reflect.ParameterizedType;
import org.hibernate.SessionFactory;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author mauricio
 */
public abstract class GenericDAO<T> {

    private final Class<T> clasePersistente;
    private final SessionFactory sessionFactory;

    public GenericDAO() {
        this.clasePersistente = (Class<T>) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        sessionFactory = HibernateUtil.getSessionFactory();        
    }

    protected Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    public T findById(final Long id) {
        T entity;
        Session session = getSessionFactory().openSession();
        entity = (T) session.get(this.clasePersistente, id);
        session.close();
        return entity;
    }

    public List<T> getAll() {
        String query = "FROM " + clasePersistente.getName();
        Session session = getSessionFactory().openSession();
        List<T> returnList = session.createQuery(query).list();
        session.close();
        return returnList;
    }

    public void remove(final T object) {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.delete(object);
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
            System.out.println(e.getMessage());
        } finally {
            session.close();
        }
    }

    public T save(final T object) {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.save(object);
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
            System.out.println(e.getMessage());
        } finally {
            session.close();
        }
        return object;
    }

    public T update(final T object) {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.saveOrUpdate(object);
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
            System.out.println(e.getMessage());
        } finally {
            session.close();
        }
        return object;
    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }
}
