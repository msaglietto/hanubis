/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.auratus.dao;

import ar.com.auratus.bean.Posteo;
import ar.com.auratus.bean.Vocabulario;
import ar.com.auratus.search.BusquedaVocabulario;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author mauricio
 */
public class VocabularioDAO extends GenericDAO<Vocabulario> {

    private HashMap<String, Vocabulario> hashVocabulario;

    public void initHashVocabulario(){
        if (hashVocabulario == null) {
            hashVocabulario = new HashMap<String, Vocabulario>();
            Session session = getSessionFactory().openSession();
            List<Vocabulario> resultado = session.createCriteria(Vocabulario.class).list();
            session.close();
            for (Vocabulario vocabulario : resultado) {
                hashVocabulario.put(vocabulario.getPalabra(), vocabulario);
            }
        }
    }

    public HashMap<String, Vocabulario> getHashVocabulario() {
        return hashVocabulario;
    }

    public void setHashVocabulario(HashMap<String, Vocabulario> hashVocabulario) {
        this.hashVocabulario = hashVocabulario;
    }

    public void saveOrUpdateHashVocabulario() {
        Set<String> palabras = hashVocabulario.keySet();
        for (String palabra : palabras) {
            Vocabulario vocabulario = hashVocabulario.get(palabra);
            if (vocabulario != null
                    && vocabulario.getCantidadDocumentos() > 0
                    && !vocabulario.getPosteoCollection().isEmpty()
                    && vocabulario.getPalabra() != null) {
                Transaction tx = null;
                Session session = getSessionFactory().openSession();
                try {
                    tx = session.beginTransaction();
                    session.saveOrUpdate(vocabulario);

                    for (Posteo posteo : vocabulario.getPosteoCollection()) {
                        if (posteo.getDocumentoID() != null && posteo.getFrecuencia() > 0) {
                            posteo.setVocabularioID(vocabulario);
                            session.save(posteo);
                        }
                    }
                    tx.commit();
                } catch (Exception e) {
                    tx.rollback();
                    System.out.println(e.getMessage());
                } finally {
                    session.close();
                }
            }
        }
    }

    public List<Vocabulario> find(String palabra) {
        String qry = "FROM Vocabulario WHERE palabra LIKE '" + palabra + "'";
        Session session = getSessionFactory().openSession();
        List<Vocabulario> resultado = session.createQuery(qry).list();
        //session.close();
        return resultado;
    }

    public List<Vocabulario> findSubString(String palabra) {
        String qry = "FROM Vocabulario WHERE palabra LIKE '%" + palabra + "%'";
        Session session = getSessionFactory().openSession();
        List<Vocabulario> resultado = session.createQuery(qry).list();
        for (Vocabulario vocabulario : resultado) {
            if (vocabulario.getPalabra().equals(palabra)) {
                resultado.remove(vocabulario);
                break;
            }
        }
        //session.close();
        return resultado;
    }

    public List<BusquedaVocabulario> obtenerConsultaBusquedaOrdenadaPorNr(String consultaBusqueda) {
        StringTokenizer palabrasBusqueda = new StringTokenizer(consultaBusqueda);//divide el texto en palabras

        List<BusquedaVocabulario> consultaOrdenada = new ArrayList<BusquedaVocabulario>();

        //por cada palabra
        while (palabrasBusqueda.hasMoreElements()) {
            String palabraBusqueda = palabrasBusqueda.nextElement().toString();
            List<Vocabulario> vocabulariosExactos = find(palabraBusqueda);
            if (vocabulariosExactos != null && !vocabulariosExactos.isEmpty()) {
                for (Vocabulario vocabulario : vocabulariosExactos) {
                    BusquedaVocabulario busquedaVocabulario = new BusquedaVocabulario();
                    busquedaVocabulario.setVocabulario(vocabulario);
                    busquedaVocabulario.setExacto(true);
                    busquedaVocabulario.setProporcionCoincidencia(palabraBusqueda.length()/vocabulario.getPalabra().length());
                    consultaOrdenada.add(busquedaVocabulario);
                }
            }
            List<Vocabulario> vocabulariosSubString = findSubString(palabraBusqueda);
            if (vocabulariosSubString != null && !vocabulariosSubString.isEmpty()) {
                for (Vocabulario vocabulario : vocabulariosSubString) {
                    BusquedaVocabulario busquedaVocabulario = new BusquedaVocabulario();
                    busquedaVocabulario.setVocabulario(vocabulario);
                    busquedaVocabulario.setSubString(true);
                    busquedaVocabulario.setProporcionCoincidencia(palabraBusqueda.length()/vocabulario.getPalabra().length());
                    consultaOrdenada.add(busquedaVocabulario);
                }
            }
        }

        return consultaOrdenada;
    }

    public void saveOrUpdateVocabulariosYPosteos(List<Vocabulario> saveOrUpdateVocabulario, Collection<Posteo> values) {

        Transaction tx = null;
        Session session = getSessionFactory().openSession();
        try {
            tx = session.beginTransaction();
            for (Vocabulario vocabulario : saveOrUpdateVocabulario) {
                session.saveOrUpdate(vocabulario);
            }

            for (Posteo posteo : values) {
                session.save(posteo);
            }
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
            System.out.println(e.getMessage());
        } finally {
            session.close();
        }

    }    
}

