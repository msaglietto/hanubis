/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.auratus.dao;

import ar.com.auratus.bean.Documento;
import java.util.List;
import org.hibernate.Session;

/**
 *
 * @author mauricio
 */
public class DocumentoDAO extends GenericDAO<Documento> {

    public boolean noExistUrl(String url) {
        if (getSessionFactory().openSession().createQuery("FROM Documento doc WHERE doc.url='" + url + "'").list().isEmpty()) {
            return true;//No existe la url 
        } else {
            return false;
        }
    }

    public Documento getUltimoInsert() {
        List<Documento> list = getSessionFactory().openSession().createQuery("FROM Documento doc WHERE id=(SELECT MAX(id) FROM Documento)").list();
        if (list.isEmpty()) {
            return null;
        } else {
            Documento doc = (Documento) list.get(0);
            return doc;
        }
    }

    public List<Documento> getAllDocumentToIndex() {
        String query = "FROM Documento doc WHERE doc.aIndexar = true";
        Session session = getSessionFactory().openSession();
        List<Documento> documentList = session.createQuery(query).list();
        session.close();
        return documentList;
    }
}
